﻿using System.Collections.Generic;

namespace JpZenHankakuLib
{
    public class JpZenHankaku {

        private Dictionary<string, string> zenHankakuNumDic = new Dictionary<string, string>(){ 
            {"０", "0"},  
            {"１", "1"},  
            {"２", "2"},  
            {"３", "3"},  
            {"４", "4"},  
            {"５", "5"},  
            {"６", "6"},  
            {"７", "7"},  
            {"８", "8"},  
            {"９", "9"},
            {"：", ":"},
            {"，", ","}
        };
							
        private Dictionary<string, string> hanZenkakuKanaDic = new Dictionary<string, string>(){ 
            {"｡", "。"},
            {"｢", "「"},
            {"｣", "」"},
            {"､", "、"},
            {"･", "・"},
            {"ｦ", "ヲ"},
            {"ｧ", "ァ"},
            {"ｨ", "ィ"},
            {"ｩ", "ゥ"},
            {"ｪ", "ェ"},
            {"ｫ", "ォ"},
            {"ｬ", "ャ"},
            {"ｭ", "ュ"},
            {"ｮ", "ョ"},
            {"ｯ", "ッ"},
            {"ｰ", "ー"},
            {"ｱ", "ア"},
            {"ｲ", "イ"},
            {"ｳ", "ウ"},
            {"ｴ", "エ"},
            {"ｵ", "オ"},
            {"ｶ", "カ"},
            {"ｷ", "キ"},
            {"ｸ", "ク"},
            {"ｹ", "ケ"},
            {"ｺ", "コ"},
            {"ｻ", "サ"},
            {"ｼ", "シ"},
            {"ｽ", "ス"},
            {"ｾ", "セ"},
            {"ｿ", "ソ"},
            {"ﾀ", "タ"},
            {"ﾁ", "チ"},
            {"ﾂ", "ツ"},
            {"ﾃ", "テ"},
            {"ﾄ", "ト"},
            {"ﾅ", "ナ"},
            {"ﾆ", "ニ"},
            {"ﾇ", "ヌ"},
            {"ﾈ", "ネ"},
            {"ﾉ", "ノ"},
            {"ﾊ", "ハ"},
            {"ﾋ", "ヒ"},
            {"ﾌ", "フ"},
            {"ﾍ", "ヘ"},
            {"ﾎ", "ホ"},
            {"ﾏ", "マ"},
            {"ﾐ", "ミ"},
            {"ﾑ", "ム"},
            {"ﾒ", "メ"},
            {"ﾓ", "モ"},
            {"ﾔ", "ヤ"},
            {"ﾕ", "ユ"},
            {"ﾖ", "ヨ"},
            {"ﾗ", "ラ"},
            {"ﾘ", "リ"},
            {"ﾙ", "ル"},
            {"ﾚ", "レ"},
            {"ﾛ", "ロ"},
            {"ﾜ", "ワ"},
            {"ﾝ", "ン"},
            {"ﾞ", "゛"},
            {"ﾟ", "゜"}
        };


        public JpZenHankaku() {
        }

        // 全角数字を半角数字に変換する。
        public string Zen2HankakuNum(string source) { 
            string destination = source;

            foreach (KeyValuePair<string, string> zenHankakuPair in zenHankakuNumDic) { 
                destination = destination.Replace(zenHankakuPair.Key, zenHankakuPair.Value);
            }

            return destination;
        }

        // 半角カナを全角カナに変換する
        public string Han2ZenkakuKana(string source) { 
            string destination = source;

            foreach (KeyValuePair<string, string> hanZenkakuKanaPair in hanZenkakuKanaDic) { 
                destination = destination.Replace(hanZenkakuKanaPair.Key, hanZenkakuKanaPair.Value);
            }

            return destination;
        }
    }
}
